import { FormComponent } from "src/app/components/form/form.component";
import { FilterPipe } from "./filter.pipe";

describe("FilterPipe", () => {
  it("should only return ['element1'] if filter criteria is '1'", () => {
    const pipe = new FilterPipe();

    expect(pipe.transform(new FormComponent().sampleItems, "1")).toEqual([
      "element1",
    ]);
  });
});
