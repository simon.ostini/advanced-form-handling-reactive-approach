import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filter",
})
export class FilterPipe implements PipeTransform {
  transform(value: string[], filter: string): string[] {
    if (value.length === 0) return value;

    const result: string[] = [];
    for (const item of value) {
      item.toUpperCase().indexOf(filter.toUpperCase()) > -1 &&
        result.push(item);
    }

    return result;
  }
}
