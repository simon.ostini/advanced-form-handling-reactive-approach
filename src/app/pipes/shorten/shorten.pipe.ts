import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "shorten",
})
export class ShortenPipe implements PipeTransform {
  private _defaultLimit: number = 20;

  transform(value: any, limit: number = this._defaultLimit): void {
    return value.length > limit ? value.slice(0, limit - 3) + "..." : value;
  }
}
