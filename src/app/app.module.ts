import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { FormComponent } from "./components/form/form.component";
import { ShortenPipe } from "./pipes/shorten/shorten.pipe";
import { FilterPipe } from "./pipes/filter/filter.pipe";

@NgModule({
  declarations: [AppComponent, FormComponent, ShortenPipe, FilterPipe],
  imports: [BrowserModule, ReactiveFormsModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
