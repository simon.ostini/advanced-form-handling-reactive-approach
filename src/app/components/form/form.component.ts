import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { pluck } from "rxjs/operators";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class FormComponent implements OnInit {
  public form!: FormGroup;
  public longString: string = "This is some very long text";
  public sampleItems: string[] = [
    "element1",
    "element2",
    "element3",
    "element4",
    "element5",
  ];
  public filterCriteria: string = "";

  constructor() {}

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl("test", [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      filter: new FormControl(null),
    });

    this.form.valueChanges.pipe(pluck("filter")).subscribe((data: string) => {
      this.filterCriteria = data;
    });
  }

  onSubmit(): void {
    console.log(this.form);
  }
}
